import picamera
import math
from datetime import datetime
from time import sleep
from pynput import keyboard


camera = picamera.PiCamera()
camera.framerate = 15
camera.resolution = (640,480)
preview = camera.start_preview()
preview.fullscreen = False
preview.window = (0,0,640,480)


class MyExitException(Exception): pass

#postavka  -  +  auto
#ISO       u  i    o
#shutter   h  j    k
#WB        b  n    m

def on_press(key):    
    if key == keyboard.Key.enter:
        print("Okidam fotku!")
    elif key == keyboard.Key.esc:
        print("Izlazim iz programa!")
    else:
        k = key.char 
        #ISO POSTAVKE
        ISO_default = 200
        ISO_min = 100
        ISO_max = 800
    
        if k == 'u':
            if camera.iso == 0:
                camera.iso = ISO_default
            elif camera.iso == ISO_min:
                print("ISO je vec minimalan")
            else:
                camera.iso = camera.iso - 100;
                print("ISO = {}".format(camera.iso))
        if k == 'i':
            if camera.iso == 0:
                camera.iso = ISO_default
            elif camera.iso == ISO_max:
                print("ISO je vec maksimalan")
            else:
                camera.iso = camera.iso + 100;
                print("ISO = {}".format(camera.iso))
        if k == 'o':
            camera.iso = 0
            print("automatski ISO")
        
        #BRZINA OKIDANJA POSTAVKE
        shutter_default = 1000000/125 #1/125s 
        shutter_max = math.floor(1000000/camera.framerate) #1/15s
        shutter_min = 1000000/4000 #1/4000s
        
        if k == 'h':
            if camera.shutter_speed == 0:
                camera.shutter_speed = camera.exposure_speed
            elif camera.shutter_speed == shutter_min:
                print("vrijeme ekspozicije je vec minimalno")
            else:
                camera.shutter_speed = math.ceil(camera.shutter_speed/2)
                if camera.shutter_speed < shutter_min:
                    camera.shutter_speed = shutter_min
                print("Vrijeme ekspozicije: {} mikrosekundi".format(camera.shutter_speed))
        if k == 'j':
            if camera.shutter_speed == 0:
                camera.shutter_speed = camera.exposure_speed
            elif camera.shutter_speed == shutter_max:
                print("vrijeme ekspozicije je vec maksimalno")
            else:
                camera.shutter_speed = camera.shutter_speed*2
                if camera.shutter_speed > shutter_max:
                    camera.shutter_speed = shutter_max
                print("Vrijeme ekspozicije: {} mikrosekundi".format(camera.shutter_speed))
        if k == 'k':
            camera.shutter_speed = 0
            print("automatsko vrijeme ekspozicije")
    
        #WB POSTAVKE
        if k == 'b':
            camera.awb_mode = 'sunlight'
            print("AWB mode = sunlight")
        if k == 'n':
            camera.awb_mode = 'tungsten'
            print("AWB mode = tungsten")
        if k == 'm':
            camera.awb_mode = 'auto'
            print("AWB mode = auto")
        

def on_release(key):

    if key == keyboard.Key.enter:
        currentTime = datetime.now()
        filename = "{}{}".format(currentTime.strftime('%Y%m%d-%H%M%S'),".jpg")
        directory = '/home/pi/'
        print("Spremam sliku u: {}".format(directory+filename))
        camera.capture(directory+filename)
        
    if key == keyboard.Key.esc:
        camera.stop_preview()
        raise MyExitException(key)
    pass


try:
    with keyboard.Listener(on_press=on_press, on_release=on_release) as listener:
        listener.join()
        
except MyExitException as e:
    pass
   
        